package nl.giorgos.cv;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import nl.giorgos.cv.fragments.AboutFragment;
import nl.giorgos.cv.fragments.AppsFragment;
import nl.giorgos.cv.presenters.AboutPresenter;
import nl.giorgos.cv.presenters.AppPresenter;
import nl.giorgos.cv.presenters.ExperiencePresenter;
import nl.giorgos.cv.services.JSONService;
import rx.Observer;

/**
 * Created by giorgos on 23/08/16.
 */
public class CVApplication extends Application{
    private static final String TAG = "G_CVApplication";

    @Override
    public void onCreate(){
        super.onCreate();

        final AboutPresenter aboutPresenter = new AboutPresenter(new JSONService());
        final ExperiencePresenter experiencePresenter = new ExperiencePresenter(new JSONService());
        final AppPresenter appsPresenter = new AppPresenter(new JSONService());

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

                Log.d(TAG, activity + " resumed");
                if(activity.getClass().isAssignableFrom(MainActivity.class)){
                    ((MainActivity)activity).getFragmentSubject().subscribe(new Observer<Fragment>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Fragment fragment) {
                            Log.d(TAG, "onNext");
                            Bundle args = fragment.getArguments();
                            int position = args.getInt("position");
                            switch(position){
                                case 0:
                                    aboutPresenter.setFragment((AboutFragment)fragment);
                                    aboutPresenter.refresh();
                                    break;
                                case 1:
                                    experiencePresenter.setFragment((AboutFragment)fragment);
                                    experiencePresenter.refresh();
                                    break;
                                case 2:
                                    appsPresenter.setFragment((AppsFragment) fragment);
                                    appsPresenter.refresh();
                                    break;
                                default:
                                    Log.d(TAG, "invalid position for fragment");
                            }
                        }
                    });
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                aboutPresenter.setFragment(null);
                experiencePresenter.setFragment(null);
                appsPresenter.setFragment(null);
                Log.d(TAG, "activity destroyed");
            }
        });
    }
}
