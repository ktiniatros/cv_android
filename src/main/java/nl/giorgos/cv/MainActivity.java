package nl.giorgos.cv;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.giorgos.cv.adapters.ViewPagerAdapter;
import nl.giorgos.cv.adapters.viewholders.ViewPagerStateAdapter;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.ReplaySubject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "GP_MainActivity";

    public ReplaySubject<Fragment> getFragmentSubject() {
        return fragmentSubject;
    }

    private ReplaySubject<Fragment> fragmentSubject = ReplaySubject.create();

    public ViewPagerAdapter adapter;

    @BindView(R.id.mainMaincontent)
    CoordinatorLayout mainMaincontent;

    @BindView(R.id.mainAppbar)
    AppBarLayout mainAppbar;

    @BindView(R.id.mainCollapseToolbar)
    CollapsingToolbarLayout mainCollapseToolbar;

    @BindView(R.id.mainHeader)
    ImageView mainHeader;

    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView (R.id.mainTabs)
    TabLayout mainTabs;

    @BindView(R.id.mainViewpager)
    ViewPager mainViewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        ViewPagerStateAdapter adapter = new ViewPagerStateAdapter(getSupportFragmentManager());
        mainViewpager.setAdapter(adapter);

        mainTabs.setupWithViewPager(mainViewpager);

        mainTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mainViewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
        });

        mainViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    default:
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onAttachFragment(final Fragment fragment){
        Log.d(TAG, "fragment attached");
        fragmentSubject.onNext(fragment);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tel:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + item.getTitle()));
                try{
                    startActivity(callIntent);
                }catch(SecurityException exc){
                    PermissionChecker.checkSelfPermission(this, "android.permission.CALL_PHONE");
                }
                return true;

            case R.id.email:
                ShareCompat.IntentBuilder.from(this)
                        .setType("message/rfc822")
                        .addEmailTo(item.getTitle().toString())
                        .setChooserTitle(getString(R.string.emailPickerTitle))
                        .startChooser();
                return true;

            case R.id.stackoverflow:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://stackoverflow.com/users/1087449/giorgos29cm")));
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
