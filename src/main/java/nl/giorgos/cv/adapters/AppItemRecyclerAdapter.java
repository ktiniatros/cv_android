package nl.giorgos.cv.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nl.giorgos.cv.R;
import nl.giorgos.cv.adapters.viewholders.AppViewHolder;
import nl.giorgos.cv.models.App;

/**
 * Created by giorgos on 25/07/16.
 */
public class AppItemRecyclerAdapter extends RecyclerView.Adapter<AppViewHolder>{
    private static final String TAG = "G_AppItemRecyclerAdapter";

    List<App> items = new ArrayList<>();

    public AppItemRecyclerAdapter(List<App> items) {
        this.items = items;
    }

    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_app_item, viewGroup, false);
        return new AppViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppViewHolder appViewHolder, int i) {
        App item = items.get(i);
        appViewHolder.update(item.getIcon(), item.getName(), item.getText());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void refreshWithItems(List<App> items){
        this.items.clear();
        this.items.addAll(items);
        notifyItemInserted(items.size());
    }
}
