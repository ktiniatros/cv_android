package nl.giorgos.cv.adapters;

/**
 * Created by giorgos on 22/07/16.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nl.giorgos.cv.R;
import nl.giorgos.cv.adapters.viewholders.AboutItemHolder;
import nl.giorgos.cv.models.AboutItem;

public class SimpleRecyclerAdapter extends RecyclerView.Adapter<AboutItemHolder> {
    private static final String TAG = "G_SimpleRecyclerAdapter";
    Context context;
    List<AboutItem> items = new ArrayList<>();

    public SimpleRecyclerAdapter(Context context, List<AboutItem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public AboutItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_list_item, viewGroup, false);
        return new AboutItemHolder(view);
    }

    @Override
    public void onBindViewHolder(AboutItemHolder appViewHolder, int i) {
        appViewHolder.title.setText(items.get(i).getTitle());
        appViewHolder.subTitle.setText(items.get(i).getText());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void refreshWithItems(List<AboutItem> items){
        this.items = items;
        notifyDataSetChanged();
    }

}

