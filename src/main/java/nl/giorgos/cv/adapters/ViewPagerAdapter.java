package nl.giorgos.cv.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nl.giorgos.cv.fragments.AboutFragment;
import nl.giorgos.cv.fragments.AppsFragment;
import nl.giorgos.cv.models.App;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by giorgos on 21/07/16.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = "G_ViewPagerAdapter";
    private final String[] names = {"About Me", "Experience", "Portfolio"};

    private AboutFragment leftFragment;
    private AboutFragment middleFragment;
    private AppsFragment rightFragment;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem at " + position);
        Bundle fragmentArguments = new Bundle();
        switch(position){
            case 0:
                AboutFragment aboutFragment = new AboutFragment();
                fragmentArguments.putInt("position", 0);
                aboutFragment.setArguments(fragmentArguments);
                return aboutFragment;
            case 1:
                AboutFragment experienceFragment = new AboutFragment();
                fragmentArguments.putInt("position", 1);
                experienceFragment.setArguments(fragmentArguments);
                return experienceFragment;
            case 2:
                AppsFragment appsFragment = new AppsFragment();
                fragmentArguments.putInt("position", 2);
                appsFragment.setArguments(fragmentArguments);
                return appsFragment;
            default:
                throw new IllegalStateException("Invalid page for ViewPagerAdapter");

        }
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem at " + position);
        final Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // save the appropriate reference depending on position
        switch (position) {
            case 0:
                leftFragment = (AboutFragment) createdFragment;
                break;
            case 1:
                middleFragment = (AboutFragment) createdFragment;
                break;
            case 2:
                rightFragment = (AppsFragment) createdFragment;
                break;
        }
        return createdFragment;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return names[position];
    }

    public AboutFragment getLeftFragment() {
        return leftFragment;
    }

    public AboutFragment getMiddleFragment() {
        return middleFragment;
    }

    public AppsFragment getRightFragment() {
        return rightFragment;
    }
}
