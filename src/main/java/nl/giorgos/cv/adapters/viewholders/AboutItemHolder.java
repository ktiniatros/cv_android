package nl.giorgos.cv.adapters.viewholders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import nl.giorgos.cv.R;

/**
 * Created by giorgos on 25/07/16.
 */
public class AboutItemHolder extends RecyclerView.ViewHolder{
    public CardView cardItemLayout;
    public TextView title;
    public TextView subTitle;

    public AboutItemHolder(View itemView) {
        super(itemView);

        cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);
        title = (TextView) itemView.findViewById(R.id.listitem_name);
        subTitle = (TextView) itemView.findViewById(R.id.listitem_subname);

    }
}
