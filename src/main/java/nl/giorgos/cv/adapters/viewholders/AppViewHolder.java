package nl.giorgos.cv.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.giorgos.cv.R;

/**
 * Created by giorgos on 25/07/16.
 */
public class AppViewHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.icon)
    ImageView iconView;

    @BindView(R.id.title)
    TextView titleView;

    @BindView(R.id.text)
    TextView textView;

    public AppViewHolder(View viewItem){
        super(viewItem);
        ButterKnife.bind(this, viewItem);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void update(String imagePath, String title, String text){
        Picasso.with(iconView.getContext()).load(imagePath).into(iconView);
        titleView.setText(title);
        textView.setText(Html.fromHtml(text));
    }
}
