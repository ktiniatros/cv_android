package nl.giorgos.cv.adapters.viewholders;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import nl.giorgos.cv.fragments.AboutFragment;
import nl.giorgos.cv.fragments.AppsFragment;

/**
 * Created by giorgos on 23/08/16.
 */
public class ViewPagerStateAdapter extends FragmentStatePagerAdapter{
    private static final String TAG = "G_ViewPagerStateAdapter";
    private final String[] names = {"About Me", "Experience", "Portfolio"};

    private AboutFragment leftFragment;
    private AboutFragment middleFragment;
    private AppsFragment rightFragment;


    public ViewPagerStateAdapter(FragmentManager manager){
        super(manager);
    }

    @Override
    public int getCount() {
        return names.length;
    }
    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "Get item on " + position);
        switch(position){
            case 0:
                return new AboutFragment();
            case 1:
                return new AboutFragment();
            case 2:
                return new AppsFragment();
            default:
                throw new IllegalStateException("Invalid page for ViewPagerAdapter");

        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem on " + position);
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // save the appropriate reference depending on position
        switch (position) {
            case 0:
                leftFragment = (AboutFragment) createdFragment;
                break;
            case 1:
                middleFragment = (AboutFragment) createdFragment;
                break;
            case 2:
                rightFragment = (AppsFragment) createdFragment;
                break;
        }
        return createdFragment;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return names[position];
    }

    public AboutFragment getLeftFragment() {
        return leftFragment;
    }

    public AboutFragment getMiddleFragment() {
        return middleFragment;
    }

    public AppsFragment getRightFragment() {
        return rightFragment;
    }
}
