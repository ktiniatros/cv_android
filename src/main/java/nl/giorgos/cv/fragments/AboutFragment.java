package nl.giorgos.cv.fragments;

/**
 * Created by giorgos on 25/07/16.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nl.giorgos.cv.MainActivity;
import nl.giorgos.cv.R;
import nl.giorgos.cv.adapters.SimpleRecyclerAdapter;
import nl.giorgos.cv.models.AboutItem;
import nl.giorgos.cv.presenters.AboutPresenter;
import nl.giorgos.cv.presenters.AppPresenter;
import nl.giorgos.cv.services.JSONService;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {
    private static final String TAG = "GP_AboutFragment";
    private SimpleRecyclerAdapter adapter;

    public AboutFragment() {
        List<AboutItem> list = new ArrayList<>();
        adapter = new SimpleRecyclerAdapter(getActivity(), (List)list);
        Log.d(TAG, "aboutFragment constructor");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.listView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        return view;
    }

    public void update(List<AboutItem> items){
        adapter.refreshWithItems(items);
    }

}
