package nl.giorgos.cv.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nl.giorgos.cv.MainActivity;
import nl.giorgos.cv.R;
import nl.giorgos.cv.adapters.AppItemRecyclerAdapter;
import nl.giorgos.cv.models.App;
import nl.giorgos.cv.presenters.AppPresenter;
import nl.giorgos.cv.services.JSONService;

/**
 * Created by giorgos on 25/07/16.
 */
public class AppsFragment extends Fragment {
    private static final String TAG = "G_AppsFragment";
    private AppItemRecyclerAdapter adapter;
    public AppsFragment(){
        adapter = new AppItemRecyclerAdapter(new ArrayList<App>());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.listView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);

        return view;
    }

    public void update(List<App> items){
        adapter.refreshWithItems(items);
    }
}
