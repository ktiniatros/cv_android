package nl.giorgos.cv.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nl.giorgos.cv.R;

/**
 * Created by giorgos on 25/07/16.
 */
public class GPFragment extends Fragment {
    private RecyclerView.Adapter adapter;

    public GPFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.listView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        Bundle arguments = getArguments();
        String adapterType = arguments.getString("adapter");

        try{
            adapter = (RecyclerView.Adapter)Class.forName(adapterType).newInstance();
            recyclerView.setAdapter(adapter);
        }catch (Exception exc){
            exc.printStackTrace();
        }

        return view;
    }
}
