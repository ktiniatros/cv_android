package nl.giorgos.cv.interfaces;

import java.util.List;

import rx.Observable;
import retrofit.http.GET;

import nl.giorgos.cv.models.AboutItem;
import nl.giorgos.cv.models.App;

/**
 * Created by giorgos on 25/07/16.
 */
public interface JSONApi {

    @GET("/about.json")
    Observable<List<AboutItem>>
    getAboutItems();

    @GET("/experience.json")
    Observable<List<AboutItem>>
    getExperienceItems();

    @GET("/apps.json")
    Observable<List<App>>
    getApps();
}
