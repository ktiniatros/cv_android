package nl.giorgos.cv.models;

/**
 * Created by giorgos on 25/07/16.
 */
public class AboutItem {

    private String title;
    private String text;

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }


    public void setTitle(String title) {
        this.title = title;
    }
    public void setText(String text) {
        this.text = text;
    }
}
