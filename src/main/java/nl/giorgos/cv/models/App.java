package nl.giorgos.cv.models;

/**
 * Created by giorgos on 23/07/16.
 */
public class App {

    private String name;
    private String icon;
    private String text;

    public String getIcon() {
        return icon;
    }
    public String getName() {
        return name;
    }
    public String getText() {
        return text;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
    public void setText(String text) {
        this.text = text;
    }
}
