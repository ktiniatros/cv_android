package nl.giorgos.cv.presenters;

import nl.giorgos.cv.fragments.AboutFragment;
import nl.giorgos.cv.models.AboutItem;
import nl.giorgos.cv.services.JSONService;
import rx.Observable;

import java.util.Calendar;
import java.util.List;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by giorgos on 25/07/16.
 */
public class AboutPresenter {
    private static final String TAG = "G_AboutPresenter";
    private JSONService jsonService;
    private static final int UPDATE_INTERVAL = 60000;
    private long lastUpdateTime = Calendar.getInstance().getTimeInMillis();

    private AboutFragment fragment;
    private Observable observable;

    public AboutPresenter(JSONService jsonService){
        this.jsonService = jsonService;
        this.observable = jsonService.getApi().getAboutItems().cache();
    }

    public void setFragment(AboutFragment fragment) {
        this.fragment = fragment;
    }

    public AboutFragment getFragment() {
        return fragment;
    }

    public void refresh() {
        long now = Calendar.getInstance().getTimeInMillis();
        if(now - lastUpdateTime > UPDATE_INTERVAL){
            lastUpdateTime = now;
            observable = jsonService.getApi().getAboutItems();
        }
        observable
        .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AboutItem>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<AboutItem> aboutItems) {
                        try{
                            fragment.update(aboutItems);
                        }catch (Exception exc){
                            exc.printStackTrace();
                        }
                    }
                });
    }
}