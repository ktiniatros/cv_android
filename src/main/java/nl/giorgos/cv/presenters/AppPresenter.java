package nl.giorgos.cv.presenters;

import java.util.Calendar;
import java.util.List;

import nl.giorgos.cv.fragments.AppsFragment;
import nl.giorgos.cv.models.App;
import nl.giorgos.cv.services.JSONService;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by giorgos on 25/07/16.
 */
public class AppPresenter {
    private static final String TAG = "G_AppPresenter";
    private static final int UPDATE_INTERVAL = 60000;
    private long lastUpdateTime = Calendar.getInstance().getTimeInMillis();
    JSONService jsonService;

    public AppsFragment getFragment() {
        return fragment;
    }

    public void setFragment(AppsFragment fragment) {
        this.fragment = fragment;
    }

    private AppsFragment fragment;

    public AppPresenter(JSONService jsonService){
        this.jsonService = jsonService;
    }

    public void refresh() {
        Observable<List<App>> observable = jsonService.getApi().getApps().cache();
        long now = Calendar.getInstance().getTimeInMillis();
        if(now - lastUpdateTime > UPDATE_INTERVAL){
            lastUpdateTime = now;
            observable = jsonService.getApi().getApps();
        }
        observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<App>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<App> items) {
                        try{
                            fragment.update(items);
                        }catch (Exception exc){
                            exc.printStackTrace();
                        }
                    }
                });
    }
}
