package nl.giorgos.cv.services;

import nl.giorgos.cv.interfaces.JSONApi;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by giorgos on 25/07/16.
 */
public class JSONService {
    private static final String BASE_URL = "http://www.giorgos.nl/apps/me";

    private JSONApi api;

    public JSONService(){
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(requestInterceptor)
//                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        api = restAdapter.create(JSONApi.class);
    }

    public JSONApi getApi(){
        return api;
    }
}
